jQuery(document).ready(function() {
    var numberOfItems;
    //import header, footer
    $("#header").load("/includes/header.html");
    $("#footer").load("/includes/footer.html");

    // for hover dropdown menu
    // $('ul.nav li.dropdown').hover(function() {
    //     $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
    // }, function() {
    //     $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
    // });

    //pagination
    $(function() {
        paginationCategory($(".active .item").length);
    });


    // slick slider call 
    $('.slick_slider').slick({
        dots: true,
        infinite: true,
        speed: 800,
        slidesToShow: 1,
        slide: 'div',
        autoplay: true,
        autoplaySpeed: 5000,
        cssEase: 'linear'
    });

    // slick slider call
    $('.owl-carousel').slick({
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 3,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: true,
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                arrows: true,
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1
            }
        }]
    });
    // latest post slider call 
    // $('.latest_postnav').newsTicker({
    //     row_height: 64,
    //     speed: 800,
    //     prevButton: $('#prev-button'),
    //     nextButton: $('#next-button')
    // });
    jQuery(".fancybox-buttons").fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: true,
        helpers: {
            title: {
                type: 'inside'
            },
            buttons: {}
        }
    });
    // jQuery('a.gallery').colorbox();
    //Check to see if the window is top if not then display button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
    //Click event to scroll to top
    $('.scrollToTop').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    $('.tootlip').tooltip();
    $("ul#ticker01").liScroll();
});

// wow = new WOW({
//     animateClass: 'animated',
//     offset: 100
// });
// wow.init();

jQuery(window).load(function() { // makes sure the whole site is loaded
    $('#status').fadeOut(); // will first fade out the loading animation
    //$('#preloader').delay(100).fadeOut('slow'); // will fade out the white DIV that covers the website.
    // $('body').delay(100).css({
    //     'overflow': 'visible'
    // });
})

function countItems(items) {
    var id = items.dataset.target;
    numberOfItems = $(id + " .item").length;
    paginationCategory($(id + " .item").length);
}

function getPageList(totalPages, page, maxLength) {
    if (maxLength < 2) throw "maxLength must be at least 2";

    function range(start, end) {
        return Array.from(Array(end - start + 1), (_, i) => i + start);
    }

    var sideWidth = maxLength < 9 ? 1 : 2;
    var leftWidth = (maxLength - sideWidth * 2 - 3) >> 1;
    var rightWidth = (maxLength - sideWidth * 2 - 2) >> 1;
    if (totalPages <= maxLength) {
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        return range(1, maxLength - sideWidth - 1)
            .concat([0])
            .concat(range(totalPages - sideWidth + 1, totalPages));
    }
    if (page >= totalPages - sideWidth - 1 - rightWidth) {
        return range(1, sideWidth)
            .concat([0])
            .concat(range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages));
    }
    return range(1, sideWidth)
        .concat([0])
        .concat(range(page - leftWidth, page + rightWidth))
        .concat([0])
        .concat(range(totalPages - sideWidth + 1, totalPages));
}

function paginationCategory(numberOfItems) {
    var limitPerPage = 3;
    var totalPages = Math.ceil(numberOfItems / limitPerPage);
    var paginationSize = 10;
    var currentPage;

    function showPage(whichPage) {
        if (whichPage < 1 || whichPage > totalPages) return false;
        currentPage = whichPage;
        $(".active .item").hide()
            .slice((currentPage - 1) * limitPerPage,
                currentPage * limitPerPage).show();
        $(".pagination li").slice(1, -1).remove();
        getPageList(totalPages, currentPage, paginationSize).forEach(item => {
            $("<li>").addClass("page-item")
                .addClass(item ? "current-page" : "disabled")
                .toggleClass("active", item === currentPage).append(
                    $("<a>").addClass("page-link").attr({
                        href: "javascript:void(0)"
                    }).text(item || "...")
                ).insertBefore("#next-page");
        });
        $("#previous-page").toggleClass("disabled", currentPage === 1);
        $("#next-page").toggleClass("disabled", currentPage === totalPages);
        return true;
    }
    $(".pagination").append(
        $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
            $("<a>").addClass("page-link").attr({
                href: "javascript:void(0)"
            }).html("&lt;")
        ),
        $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
            $("<a>").addClass("page-link").attr({
                href: "javascript:void(0)"
            }).html("&gt;")
        )
    );
    $(".active .items").show();
    showPage(1);

    $(document).on("click", ".pagination li.current-page:not(.active)", function() {
        return showPage(+$(this).text());
    });
    $("#next-page").on("click", function() {
        return showPage(currentPage + 1);
    });

    $("#previous-page").on("click", function() {
        return showPage(currentPage - 1);
    });
    // });
}